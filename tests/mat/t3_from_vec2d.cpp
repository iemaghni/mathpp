#include "mathpp/Mat.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	constexpr auto r =
	    m::mat::from_vec2d<int, 3>({{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
	auto ok = true;
	ok &= r[0][0] == 1 && r[0][1] == 2 && r[0][2] == 3;
	ok &= r[1][0] == 4 && r[1][1] == 5 && r[1][2] == 6;
	ok &= r[2][0] == 7 && r[2][1] == 8 && r[2][2] == 9;
	return ok ? t::OK : t::FAIL;
}
