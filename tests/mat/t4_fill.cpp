#include "mathpp/Mat.hpp"

#include "../t.hpp"
#include "mathpp/Vec.hpp"

namespace m = mathpp;

int main() {
	constexpr auto r = m::Mat<int, 3>::fill(42);
	auto ok = true;
	ok &= r[0][0] == 42 && r[0][1] == 42 && r[0][2] == 42;
	ok &= r[1][0] == 42 && r[1][1] == 42 && r[1][2] == 42;
	ok &= r[2][0] == 42 && r[2][1] == 42 && r[2][2] == 42;
	return ok ? t::OK : t::FAIL;
}
