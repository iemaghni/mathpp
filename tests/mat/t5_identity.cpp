#include "mathpp/Mat.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	constexpr auto r = m::Mat<int, 3>::identity();
	auto ok = true;
	ok &= r[0][0] == 1 && r[0][1] == 0 && r[0][2] == 0;
	ok &= r[1][0] == 0 && r[1][1] == 1 && r[1][2] == 0;
	ok &= r[2][0] == 0 && r[2][1] == 0 && r[2][2] == 1;
	return ok ? t::OK : t::FAIL;
}
