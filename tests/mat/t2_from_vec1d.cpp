#include "mathpp/Mat.hpp"

#include "../t.hpp"
#include "mathpp/Vec.hpp"

namespace m = mathpp;

int main() {
	constexpr auto r = m::mat::from_vec1d<int, 3>({1, 2, 3});
	auto ok = true;
	ok &= r[0][0] == 1 && r[0][1] == 2 && r[0][2] == 3;
	ok &= r[1][0] == 1 && r[1][1] == 2 && r[1][2] == 3;
	ok &= r[2][0] == 1 && r[2][1] == 2 && r[2][2] == 3;
	return ok ? t::OK : t::FAIL;
}
