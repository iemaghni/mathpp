#include "mathpp/Mat.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	constexpr auto a = m::mat::from_vec2d<int, 3, 2>({{1, 2}, {3, 4}, {5, 6}});
	auto r = a.transpose();
	auto ok = true;
	ok &= r[0][0] == 1 && r[0][1] == 3 && r[0][2] == 5;
	ok &= r[1][0] == 2 && r[1][1] == 4 && r[1][2] == 6;
	return ok ? t::OK : t::FAIL;
}
