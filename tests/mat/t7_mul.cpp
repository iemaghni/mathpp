#include "mathpp/Mat.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	constexpr auto m1 = m::mat::from_vec2d<int, 2, 3>({{1, 2, 3}, {4, 5, 6}});
	constexpr auto m2 = m::mat::from_vec2d<int, 3, 2>({{1, 2}, {3, 4}, {5, 6}});
	auto r = m1 * m2;
	auto ok = r[0][0] == 22 && r[0][1] == 28 && r[1][0] == 49 && r[1][1] == 64;
	return ok ? t::OK : t::FAIL;
}
