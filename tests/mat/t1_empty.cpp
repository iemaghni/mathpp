#include "mathpp/Mat.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	constexpr auto r = m::mat::empty<int, 3>();
	auto ok = true;
	ok &= r[0][0] == 0 && r[0][1] == 0 && r[0][2] == 0;
	ok &= r[1][0] == 0 && r[1][1] == 0 && r[1][2] == 0;
	ok &= r[2][0] == 0 && r[2][1] == 0 && r[2][2] == 0;
	return ok ? t::OK : t::FAIL;
}
