#include "mathpp/cmp.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	return m::min(42, 69) == 42 ? t::OK : t::FAIL;
}
