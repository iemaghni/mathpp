#include "mathpp/cmp.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	return m::max(42, 69) == 69 ? t::OK : t::FAIL;
}
