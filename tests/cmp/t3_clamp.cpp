#include "mathpp/cmp.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
  constexpr auto mm = m::clamp(2, 4);
  auto ok = true;
  ok &= mm(1) == 2 && mm(2) == 2;
  ok &= mm(3) == 3;
  ok &= mm(4) == 4 && mm(5) == 4;
	return ok ? t::OK : t::FAIL;
}
