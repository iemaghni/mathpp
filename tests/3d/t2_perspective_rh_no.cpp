#include "mathpp/3d.hpp"
#include "mathpp/Mat.hpp"
#include "mathpp/cst.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	auto e = m::mat::from_vec2d<double, 4>(
	    {{2.41421, 0, 0, 0}, {0, 2.41421, 0, 0}, {0, 0, -1, -1}, {0, 0, 0, 0}});
	auto r = m::rh::no::perspective(m::cst::pi<double> / 4, 1.0, 0.0, 100.0);
	auto ok = true;
	auto eps = 1.0 / (1UL << 9U);

	ok &= t::cmp(r[0][0], e[0][0], eps);
	ok &= t::cmp(r[0][1], e[0][1], eps);
	ok &= t::cmp(r[0][2], e[0][2], eps);
	ok &= t::cmp(r[0][3], e[0][3], eps);

	ok &= t::cmp(r[1][0], e[1][0], eps);
	ok &= t::cmp(r[1][1], e[1][1], eps);
	ok &= t::cmp(r[1][2], e[1][2], eps);
	ok &= t::cmp(r[1][3], e[1][3], eps);

	ok &= t::cmp(r[2][0], e[2][0], eps);
	ok &= t::cmp(r[2][1], e[2][1], eps);
	ok &= t::cmp(r[2][2], e[2][2], eps);
	ok &= t::cmp(r[2][3], e[2][3], eps);

	ok &= t::cmp(r[3][0], e[3][0], eps);
	ok &= t::cmp(r[3][1], e[3][1], eps);
	ok &= t::cmp(r[3][2], e[3][2], eps);
	ok &= t::cmp(r[3][3], e[3][3], eps);

	return ok ? t::OK : t::FAIL;
}
