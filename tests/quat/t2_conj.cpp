#include "mathpp/Quat.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	auto r = m::Quat<>::from_vec4({1, 0, 1, 0}).conj();
	auto k = r.k();
	auto v = r.v();
	return (k == 1 && v[0] == 0 && v[1] == -1 && v[2] == 0) ? t::OK : t::FAIL;
}
