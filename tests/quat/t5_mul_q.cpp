#include "mathpp/Quat.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	auto e = m::Quat<>::from_vec4({0.5, 1.25, 1.5, 0.25});
	auto r = m::Quat<>::from_vec4({1, 0, 1, 0}) *
	         m::Quat<>::from_vec4({1, 0.5, 0.5, 0.75});
	auto ok = t::cmp(r.k(), e.k());
	ok &= t::cmp(r.v()[0], e.v()[0]);
	ok &= t::cmp(r.v()[1], e.v()[1]);
	ok &= t::cmp(r.v()[2], e.v()[2]);
	return ok ? t::OK : t::FAIL;
}
