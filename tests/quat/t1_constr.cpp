#include "mathpp/Quat.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	auto r = m::Quat<>::from(1, {});
	return t::cmp(r.k(), 0.87758255F) ? t::OK : t::FAIL;
}
