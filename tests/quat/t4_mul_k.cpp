#include "mathpp/Quat.hpp"
#include "mathpp/cst.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	auto pi = m::cst::pi<m::Float>;
	auto r = m::Quat<>::from_vec4({1, 0, 1, 0}) * pi;
	auto k = r.k();
	auto v = r.v();
	auto ok = t::cmp(k, pi) && v[0] == 0 && t::cmp(v[1], pi) && v[2] == 0;
	return ok ? t::OK : t::FAIL;
}
