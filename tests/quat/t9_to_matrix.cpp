#include "mathpp/Mat.hpp"
#include "mathpp/Quat.hpp"
#include "mathpp/cst.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	auto e =
	    m::mat::from_vec2d<m::Float, 3>({{1, 0, 0}, {0, 0, -1}, {0, 1, 0}});
	auto r = m::Quat<>::from(m::cst::half::pi<m::Float>, {1, 0, 0}).to_matrix();
	auto ok = true;
	ok &= t::cmp(r[0][0], e[0][0]) && t::cmp(r[0][1], e[0][1]) &&
	      t::cmp(r[0][2], e[0][2]);
	ok &= t::cmp(r[1][0], e[1][0]) && t::cmp(r[1][1], e[1][1]) &&
	      t::cmp(r[1][2], e[1][2]);
	ok &= t::cmp(r[2][0], e[2][0]) && t::cmp(r[2][1], e[2][1]) &&
	      t::cmp(r[2][2], e[2][2]);
	return ok ? t::OK : t::FAIL;
}
