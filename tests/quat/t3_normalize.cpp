#include "mathpp/Quat.hpp"
#include "mathpp/cst.hpp"

#include "mathpp/fn/trigo.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	auto e = m::Quat<>::from(m::cst::half::pi<m::Float>, {0, 1, 0});
	auto r = m::Quat<>::from_vec4({1, 0, 1, 0}).normalize();
	auto ok = t::cmp(r.k(), e.k());
	ok &= t::cmp(r.v()[0], e.v()[0]);
	ok &= t::cmp(r.v()[1], e.v()[1]);
	ok &= t::cmp(r.v()[2], e.v()[2]);
	return ok ? t::OK : t::FAIL;
}
