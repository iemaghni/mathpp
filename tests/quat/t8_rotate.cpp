#include "mathpp/Quat.hpp"
#include "mathpp/cst.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	constexpr auto e = m::Quat<>::from_vec4({0, 0, 1, 0});
	constexpr auto a = m::Quat<>::from_vec3({1, 0, 0});
	auto b = m::Quat<>::from(m::cst::half::pi<m::Float>, {0, 0, 1});
	auto r = a.rotate(b);
	auto ok = t::cmp(r.k(), e.k());
	ok &= t::cmp(r.v()[0], e.v()[0]);
	ok &= t::cmp(r.v()[1], e.v()[1]);
	ok &= t::cmp(r.v()[2], e.v()[2]);
	return ok ? t::OK : t::FAIL;
}
