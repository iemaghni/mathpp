#include "mathpp/Quat.hpp"

#include "../t.hpp"

namespace m = mathpp;

int main() {
	constexpr auto e = m::Quat<>::from_vec4({0, 2, 2, -2});
	constexpr auto a = m::Quat<>::from_vec3({1, 1, 1});
	constexpr auto b = m::Quat<>::from_vec4({1, 0, 1, 0});
	constexpr auto r = a.rotate(b);
	auto ok = t::cmp(r.k(), e.k());
	ok &= t::cmp(r.v()[0], e.v()[0]);
	ok &= t::cmp(r.v()[1], e.v()[1]);
	ok &= t::cmp(r.v()[2], e.v()[2]);
	return ok ? t::OK : t::FAIL;
}
