#include "../include/mathpp/Mat.hpp"

#include <iostream>

namespace m = mathpp;

int main() {
	auto m = m::mat::look_at(m::Vec3{4.0, 3.0, 3.0});
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			std::cout << " " << m[i][j];
		}
		std::cout << std::endl;
	};
}
