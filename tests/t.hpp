#ifndef T_HPP
#define T_HPP

#include "mathpp/cst.hpp"
#include "mathpp/fn/abs.hpp"

namespace t {

namespace m = mathpp;

enum { OK, FAIL, SKIP = 77, FAIL_FORCE = 99 };

template <typename T>
constexpr bool cmp(T a, T b, T eps = 2 * m::cst::eps<T>) noexcept {
	return m::fn::abs(a - b) < eps;
}

} // namespace t

#endif /* T_HPP */
