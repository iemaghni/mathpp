#ifndef MATHPP_ENSURE_HPP
#define MATHPP_ENSURE_HPP

#include <string_view>

extern "C" {
#include <stdlib.h> /* NOLINT abort() */

#include <unistd.h> /* STDERR_FILENO write() */
}

namespace mathpp {

#if defined(NDEBUG)
constexpr void ensure(bool, std::string_view) noexcept {}
#else
constexpr void ensure(bool ok, std::string_view s) noexcept {
	if (!ok) [[unlikely]] {
		::write(STDERR_FILENO, "[mathpp] ", 9);
		::write(STDERR_FILENO, s.data(), s.size());
		::write(STDERR_FILENO, "\n", 1);
		::abort();
	}
}
#endif

} // namespace mathpp

#endif /* MATHPP_ENSURE_HPP */
