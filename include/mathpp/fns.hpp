#ifndef MATHPP_FNS_HPP
#define MATHPP_FNS_HPP

#include "Vec.hpp"
#include "cst.hpp"
#include "fn/mod.hpp"
#include "fn/remainder.hpp"

namespace mathpp {

/* [-PI;+PI] */
template <typename T> constexpr auto wrap_to_pi(T rad) noexcept {
	return fn::remainder(rad, 2 * cst::pi<T>);
}

/* [0;2*PI[ */
template <typename T> constexpr auto wrap_to_2pi(T rad) noexcept {
	return fn::mod(rad, 2 * cst::pi<T>);
}

template <typename T> constexpr auto rad_to_deg(T rad) noexcept {
	return rad * T(180) * cst::inv::pi<T>;
}

template <typename T> constexpr auto deg_to_rad(T deg) noexcept {
	return deg * cst::pi<T> / T(180);
}

template <typename T>
constexpr auto quadratic_alt(T a_minus, T b_half, T c) noexcept {
	auto disc = b_half ^ 2 + a_minus * c;
	auto sq = sqrt(disc);
	return Vec<T, 2>({b_half - sq, b_half + sq});
}

template <typename T> constexpr auto quadratic(T a, T b, T c) noexcept {
	return quadratic_alt(-a, b / 2, c);
}

template <typename T> constexpr auto gcf(T x, T y) {
	static_assert(x < y, "x < y");
	while (x != 0) {
		auto z = x;
		x = fn::mod(y, x);
		y = z;
	}
	return y;
}

} // namespace mathpp

#endif /* MATHPP_FNS_HPP */
