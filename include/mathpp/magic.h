#ifndef MATHPP_MAGIC_H
#define MATHPP_MAGIC_H

/* This is a C file! */

#include <stdint.h>

/* From http://www.lomont.org/papers/2003/InvSqrt.pdf */
inline float fast_inv_sqrtf(float x) {
	float x_half = 0.5F * x;
	uint32_t i = *(uint32_t *)&x; /* get bits for floating value */
	i = 0x5F375A86U - (i >> 1U);  /* gives initial guess y0 */
	x = *(float *)&i;             /* convert bits back to float */
	x *= (1.5F - x_half * x * x); /* repeat Newton, step increases accuracy */
	return x;
}

#endif /* MATHPP_MAGIC_H */
