#ifndef MATHPP_CMP_HPP
#define MATHPP_CMP_HPP

namespace mathpp {

constexpr auto min = [](auto x, auto y) noexcept { return x < y ? x : y; };

constexpr auto max = [](auto x, auto y) noexcept { return x > y ? x : y; };

constexpr auto clamp = [](auto l, auto h) noexcept { return [=](auto x) noexcept { return min(max(x, l), h); }; };

} // namespace mathpp

#endif /* MATHPP_CMP_HPP */
