#ifndef MATHPP_3D_HPP
#define MATHPP_3D_HPP

#include "Mat.hpp"
#include "Vec.hpp"
#include "cst.hpp"
#include "ensure.hpp"
#include "fn/abs.hpp"

#include <utility> /* move() */

// Right-handed
namespace mathpp::rh {

// https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/lookat-function
// A more elegant solution can be developed using quaternion interpolation.

// Based on lookAtRH()
template <typename T>
constexpr auto look_at(Vec<T, 3> &&eye, Vec<T, 3> &&center = {},
                       Vec<T, 3> up = {0, 1, 0}) noexcept {
	auto f = vec::normalize(vec::sub(std::move(center), eye));
	auto s = vec::normalize(vec::cross(f, up));
	auto u = vec::cross(s, f);

	f = vec::mul<T>(std::move(f), -T(1));

	auto d = vec::mul<T>(
	    Vec<T, 4>{vec::dot(s, eye), vec::dot(u, eye), vec::dot(f, eye), -T(1)},
	    -T(1));

	return mat::from_vec2d<T, 4>({{s[0], u[0], f[0], 0},
	                              {s[1], u[1], f[1], 0},
	                              {s[2], u[2], f[2], 0},
	                              std::move(d)});
}

/* Negative-one */
namespace no {

// Based on perspectiveRH_NO()
template <typename T>
constexpr auto perspective(T fovy, T aspect, T zNear, T zFar) noexcept {
	ensure(fn::abs(aspect - cst::eps<T>) > T{}, "perspective()");
	auto tanHalfFovy = tan(fovy / T(2));
	return mat::from_vec2d<T, 4>(
	    {{T(1) / (aspect * tanHalfFovy), 0, 0, 0},
	     {0, T(1) / tanHalfFovy, 0, 0},
	     {0, 0, -(zFar + zNear) / (zFar - zNear), -T(1)},
	     {0, 0, -(T(2) * zFar * zNear) / (zFar - zNear), 0}});
}

} // namespace no

// Zero-to-one
namespace zo {
// Based on perspectiveRH_ZO()
template <typename T>
constexpr auto perspective(T fovy, T aspect, T zNear, T zFar) noexcept {
	ensure(fn::abs(aspect - cst::eps<T>) > T{}, "perspective()");
	auto tanHalfFovy = tan(fovy / T(2));
	return mat::from_vec2d<T, 4>({{T(1) / (aspect * tanHalfFovy), 0, 0, 0},
	                              {0, T(1) / tanHalfFovy, 0, 0},
	                              {0, 0, zFar / (zNear - zFar), T(1)},
	                              {0, 0, -(zFar * zNear) / (zFar - zNear), 0}});
}

} // namespace zo

} // namespace mathpp::rh

#endif /* MATHPP_3D_HPP */
