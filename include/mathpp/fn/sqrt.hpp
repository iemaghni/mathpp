#ifndef MATHPP_FN_SQRT_HPP
#define MATHPP_FN_SQRT_HPP

extern "C" {
#include <math.h> /* NOLINT sqrt*() */
}

namespace mathpp::fn {

constexpr auto sqrt(float x) noexcept { return ::sqrtf(x); }

constexpr auto sqrt(double x) noexcept { return ::sqrt(x); }

constexpr auto sqrt(long double x) noexcept { return ::sqrtl(x); }

} // namespace mathpp::fn

#endif /* MATHPP_FN_SQRT_HPP */
