#ifndef MATHPP_FN_MOD_HPP
#define MATHPP_FN_MOD_HPP

extern "C" {
#include <math.h> /* NOLINT fmod*() */
}

namespace mathpp::fn {

constexpr auto mod(int x, int y) noexcept { return x % y; }

constexpr auto mod(long int x, long int y) noexcept { return x % y; }

constexpr auto mod(long long int x, long long int y) noexcept { return x % y; }

constexpr auto mod(float x, float y) noexcept { return ::fmodf(x, y); }

constexpr auto mod(double x, double y) noexcept { return ::fmod(x, y); }

constexpr auto mod(long double x, long double y) noexcept {
	return ::fmodl(x, y);
}

} // namespace mathpp::fn

#endif /* MATHPP_FN_MOD_HPP */
