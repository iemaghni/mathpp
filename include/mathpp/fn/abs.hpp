#ifndef MATHPP_FN_ABS_HPP
#define MATHPP_FN_ABS_HPP

extern "C" {
#include <math.h>   /* NOLINT fabs*() */
#include <stdlib.h> /* NOLINT *abs() */
}

namespace mathpp::fn {

constexpr auto abs(int x) noexcept { return ::abs(x); }

constexpr auto abs(long int x) noexcept { return ::labs(x); }

constexpr auto abs(long long int x) noexcept { return ::llabs(x); }

constexpr auto abs(float x) noexcept { return ::fabsf(x); }

constexpr auto abs(double x) noexcept { return ::fabs(x); }

constexpr auto abs(long double x) noexcept { return ::fabsl(x); }

} // namespace mathpp::fn

#endif /* MATHPP_FN_ABS_HPP */
