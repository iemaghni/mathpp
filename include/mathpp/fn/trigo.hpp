#ifndef MATHPP_FN_TRIGO_HPP
#define MATHPP_FN_TRIGO_HPP

extern "C" {
#include <math.h> /* NOLINT cos*() sin*() tan*() */
}

namespace mathpp::fn {

constexpr auto sin(float x) noexcept { return ::sinf(x); }
constexpr auto sin(double x) noexcept { return ::sin(x); }
constexpr auto sin(long double x) noexcept { return ::sinl(x); }

constexpr auto cos(float x) noexcept { return ::cosf(x); }
constexpr auto cos(double x) noexcept { return ::cos(x); }
constexpr auto cos(long double x) noexcept { return ::cosl(x); }

constexpr auto tan(float x) noexcept { return ::tanf(x); }
constexpr auto tan(double x) noexcept { return ::tan(x); }
constexpr auto tan(long double x) noexcept { return ::tanl(x); }

} // namespace mathpp::fn

#endif /* MATHPP_FN_TRIGO_HPP */
