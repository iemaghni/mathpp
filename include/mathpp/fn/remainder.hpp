#ifndef MATHPP_FN_REMAINDER_HPP
#define MATHPP_FN_REMAINDER_HPP

extern "C" {
#include <math.h> /* NOLINT remainder*() */
}

namespace mathpp::fn {

constexpr auto remainder(float x, float y) noexcept {
	return ::remainderf(x, y);
}

constexpr auto remainder(double x, double y) noexcept {
	return ::remainder(x, y);
}

constexpr auto remainder(long double x, long double y) noexcept {
	return ::remainderl(x, y);
}

} // namespace mathpp::fn

#endif /* MATHPP_FN_REMAINDER_HPP */
