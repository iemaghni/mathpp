#ifndef MATHPP_FN_POW_HPP
#define MATHPP_FN_POW_HPP

extern "C" {
#include <math.h> /* NOLINT pow*() */
}

namespace mathpp::fn {

template <typename T> constexpr auto pow(T x, T y) noexcept {
	auto r = T{1};
	while (y > 0) {
		if (y % 2 != 0) {
			r *= x;
		}
		y /= 2;
		x *= x;
	}
	return r;
}

constexpr auto pow(float x, float y) noexcept { return ::powf(x, y); }

constexpr auto pow(double x, double y) noexcept { return ::pow(x, y); }

constexpr auto pow(long double x, long double y) noexcept {
	return ::powl(x, y);
}

} // namespace mathpp::fn

#endif /* MATHPP_FN_POW_HPP */
