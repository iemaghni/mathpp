#ifndef MATHPP_MAT_HPP
#define MATHPP_MAT_HPP

#include "T.hpp"
#include "Vec.hpp"

#include <utility> /* move() */

namespace mathpp {

template <typename T, USize M, USize N = M> class Mat {
private:
	Vec<Vec<T, N>, M> m_{};

	constexpr Mat() noexcept = default;
	constexpr explicit Mat(Vec<T, N> &&v) noexcept : m_(std::move(v)) {}
	constexpr explicit Mat(Vec<Vec<T, N>, M> &&m) noexcept : m_(std::move(m)) {}

public:
	// Named constructors
	static constexpr auto empty() noexcept { return Mat{}; }
	static constexpr auto from_vec1d(Vec<T, N> &&vec1d) noexcept {
		return Mat(std::move(vec1d));
	}
	static constexpr auto from_vec2d(Vec<Vec<T, N>, M> &&vec2d) noexcept {
		return Mat(std::move(vec2d));
	}
	static constexpr auto fill(T &&t) noexcept {
		return from_vec1d(Vec<T, M>(std::move(t)));
	}

	static constexpr auto identity() noexcept {
		static_assert(M == N, "M != N");
		auto m = Mat::empty();
		for (auto i = USize{}; i < M; ++i) {
			m[i][i] = 1;
		}
		return m;
	}

	constexpr auto &operator[](USize i) const noexcept { return m_[i]; };
	constexpr auto &operator[](USize i) noexcept { return m_[i]; };

	constexpr auto transpose() const noexcept {
		auto mm = Mat<T, N, M>::empty();
		for (auto i = USize{}; i < M; ++i) {
			for (auto j = USize{}; j < N; ++j) {
				mm[j][i] = (*this)[i][j];
			}
		}
		return mm;
	}

	constexpr auto operator*(const Mat<T, N, M> &m) const noexcept {
		auto mm = Mat<T, M, M>::empty();
		for (auto i = USize{}; i < M; ++i) {
			for (auto j = USize{}; j < M; ++j) {
				auto t = T{};
				for (auto k = USize{}; k < N; ++k) {
					t += (*this)[i][k] * m[k][j];
				}
				mm[i][j] = t;
			}
		}
		return mm;
	}
};

namespace mat {

template <typename T, USize M, USize N = M> constexpr auto empty() noexcept {
	return Mat<T, M, N>::empty();
}

template <typename T, USize M, USize N = M>
constexpr auto from_vec1d(Vec<T, N> &&vec1d) noexcept {
	return Mat<T, M, N>::from_vec1d(std::move(vec1d));
}

template <typename T, USize M, USize N = M>
constexpr auto from_vec2d(Vec<Vec<T, N>, M> &&vec2d) noexcept {
	return Mat<T, M, N>::from_vec2d(std::move(vec2d));
}

template <typename T, USize M, USize N = M> constexpr auto identity() noexcept {
	return Mat<T, M, N>::identity();
}

// template <USize P, USize Q = P, typename T, USize M, USize N>
// constexpr auto fill(const Mat<T, M, N> &m) noexcept {
// 	auto mm = Mat<T, P, Q>{};
// 	static_assert(P < M && Q < N, "m2 < m1");
// 	for (auto i = USize{}; i < P; ++i) {
// 		for (auto j = USize{}; j < Q; ++j) {
// 			mm[i][j] = m[i][j];
// 		}
// 	}
// 	return mm;
// }

// template <USize P, USize Q = P, typename T, USize M, USize N>
// constexpr auto fit(const Mat<T, M, N> &m, T t = {}) noexcept {
// 	auto mm = Mat<T, P, Q>{};
// 	static_assert(P > M && Q > N, "m2 > m1");
// 	for (auto i = USize{}; i < M; ++i) {
// 		for (auto j = USize{}; j < N; ++j) {
// 			mm[i][j] = m[i][j];
// 		}
// 		for (auto j = N; j < Q; ++j) {
// 			mm[i][j] = t;
// 		}
// 	}
// 	for (auto i = M; i < P; ++i) {
// 		for (auto j = USize{}; j < Q; ++j) {
// 			mm[i][j] = t;
// 		}
// 	}
// 	return mm;
// }

} // namespace mat

} // namespace mathpp

#endif /* MATHPP_MAT_HPP */
