#ifndef MATHPP_CST_HPP
#define MATHPP_CST_HPP

#include <limits> /* numeric_limits */

namespace mathpp::cst {

template <typename T> constexpr auto eps = std::numeric_limits<T>::epsilon();

template <typename T>
constexpr auto e = T(2.718281828459045235360287471352662498);
template <typename T>
constexpr auto pi = T(3.141592653589793238462643383279502884);

namespace half {
template <typename T>
constexpr auto pi = T(1.570796326794896619231321691639751442);
} // namespace half

namespace inv {
template <typename T>
constexpr auto pi = T(0.318309886183790671537767526745028724);

namespace sqrt {
template <typename T>
constexpr auto two = T(0.707106781186547524400844362104849039);
} // namespace sqrt
} // namespace inv

namespace sqrt {
template <typename T>
constexpr auto two = T(1.414213562373095048801688724209698079);
} // namespace sqrt

} // namespace mathpp::cst

#endif /* MATHPP_CST_HPP */
