#ifndef MATHPP_QUAT_HPP
#define MATHPP_QUAT_HPP

#include "Mat.hpp"
#include "T.hpp"
#include "Vec.hpp"
#include "fn/sqrt.hpp"
#include "fn/trigo.hpp"

#include <utility> /* move() */

#include <iostream>

namespace mathpp {

template <typename T = Float> class Quat {
	T k_;
	Vec<T, 3> v_;

	template <USize N>
	Vec<T, N> constexpr vmul(Vec<T, N> &&v, T k) const noexcept {
		return vec::mul(v, k), v;
	}

	constexpr Quat(T k, Vec<T, 3> &&v) noexcept : k_{k}, v_{std::move(v)} {}
	// constexpr explicit Quat(Vec<T, 3> &&v) noexcept : Quat({}, std::move(v))
	// {} constexpr explicit Quat(const Vec<T, 4> &v) noexcept
	//     : Quat(v[0], {v[1], v[2], v[3]}) {}
	// constexpr Quat(T t, Vec<T, 3> &&v) noexcept
	//     : k_{fn::cos(t / 2)}, v_{vmul(std::move(v), fn::sin(t / 2))} {}

public:
	//////////////////////////////////////////////////////////////////////////////
	// Named constructors
	// TODO():
	// static constexpr auto copy(const Quat &q) noexcept {
	// 	return Quat(q.k(), q.v()); // copy vector
	// }
	static constexpr inline auto build(T k, Vec<T, 3> &&v) noexcept {
		return Quat(k, std::move(v));
	}
	static constexpr inline auto from(T t, Vec<T, 3> &&v) noexcept {
		return Quat(fn::cos(t / 2), vec::mul(std::move(v), fn::sin(t / 2)));
	}
	static constexpr inline auto from_vec3(Vec<T, 3> &&v) noexcept {
		return Quat({}, std::move(v));
	}
	static constexpr inline auto from_vec4(const Vec<T, 4> &v) noexcept {
		return Quat(v[0], {v[1], v[2], v[3]});
	}

	//////////////////////////////////////////////////////////////////////////////
	// Methods

	constexpr inline auto k() const noexcept { return k_; }
	constexpr inline const auto &v() const noexcept { return v_; }

	constexpr auto operator*(T k2) const noexcept {
		return build(k() * k2, vec::mul(v(), k2));
	}

	constexpr auto operator*(const Quat &q) const noexcept {
		// k = (qa.raw(1) * qb.raw(1)) - dot(va, vb)
		// v = (qa.k .* qb.v) + (qb.k .* qa.v) + cross(qa.v, qb.v);
		// q = quat([k; v]);
		return build(
		    (k() * q.k()) - vec::dot(v(), q.v()),
		    vec::add(vec::add(vec::mul(q.v(), k()), vec::mul(v(), q.k())),
		             vec::cross(v(), q.v())));
	}

	constexpr auto conj() const noexcept {
		return build(k(), std::move(vec::mul(v(), T(-1))));
	}

	constexpr auto rotate(const Quat &q) const noexcept {
		return q * *this * q.conj();
	}

	// TODO(): constexpr sqrt
	auto normalize() const noexcept {
		auto disc_inv = T(1) / fn::sqrt((k() * k()) + vec::dot(v(), v()));
		return build(k() * disc_inv, std::move(vec::mul(v(), disc_inv)));
	}

	auto to_matrix() const noexcept {
		auto q = normalize();
		// // auto a = q.k();
		// // auto b = q.v()[0], c = q.v()[1], d = q.v()[2];
		// // return Mat<T, 3>::from_vec2d({{a * a + b * b - c * c - d * d, //
		// //                                2 * b * c - 2 * a * d,         //
		// //                                2 * a * c + 2 * b * d},
		// //                               {2 * a * d + 2 * b * c,         //
		// //                                a * a - b * b + c * c - d * d, //
		// //                                2 * c * d - 2 * a * b},
		// //                               {2 * b * d - 2 * a * c, //
		// //                                2 * a * b - 2 * c * d, //
		// //                                a * a - b * b - c * c + d * d}});
		auto s = q.k();
		auto x = q.v()[0], y = q.v()[1], z = q.v()[2];
		return mat::from_vec2d<T, 3>({{1 - 2 * y * y - 2 * z * z, //
		                               2 * x * y - 2 * s * z,     //
		                               2 * x * z + 2 * s * y},
		                              {2 * x * y + 2 * s * z,     //
		                               1 - 2 * x * x - 2 * z * z, //
		                               2 * y * z - 2 * s * x},
		                              {2 * x * z - 2 * s * y, //
		                               2 * y * z + 2 * s * x, //
		                               1 - 2 * x * x - 2 * y * y}});
	}
};

namespace quat {} // namespace quat

} // namespace mathpp

#endif /* MATHPP_QUAT_HPP */
