#ifndef MATHPP_T_HPP
#define MATHPP_T_HPP

extern "C" {
#include <stddef.h> /* size_t */
}

namespace mathpp {

using USize = ::size_t;

#if !defined(FLT_EVAL_METHOD) || FLT_EVAL_METHOD == 0
using Float = float;
#elif FLT_EVAL_METHOD == 1
using Float = double;
#elif FLT_EVAL_METHOD == 2
using Float = long double;
#endif

} // namespace mathpp

#endif /* MATHPP_T_HPP */
