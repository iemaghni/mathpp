#ifndef MATHPP_VEC_HPP
#define MATHPP_VEC_HPP

#include "T.hpp"
#include "fn/sqrt.hpp"

#include <initializer_list>

namespace mathpp {

template <typename T, USize N> class Vec {
	T v_[N]{}; // NOLINT

public:
	constexpr Vec() noexcept = default;

	constexpr explicit Vec(T &&t) noexcept {
		for (auto i = USize{}; i < N; ++i) {
			v_[i] = t;
		}
	}

	// TODO(): infer from argument or improve
	constexpr Vec(std::initializer_list<T> &&v) noexcept {
		// <typename T, n = v.size(V &a)>

		auto begin = v.begin();
		for (auto i = USize{}; i < N; ++i) {
			v_[i] = *begin++;
		}
	}

	constexpr auto size() const noexcept { return N; }

	constexpr auto &operator[](USize i) const noexcept { return v_[i]; };
	constexpr auto &operator[](USize i) noexcept { return v_[i]; };
};

// template <typename T, USize N> using Vec = std::array<T, N>;

// implementation-agnostic
namespace vec {

template <typename T, USize N>
constexpr auto add(Vec<T, N> &&a, const Vec<T, N> &b) noexcept {
	for (auto i = USize{}; i < a.size(); ++i) {
		a[i] += b[i];
	}
	return a;
}

template <typename T, USize N>
constexpr auto sub(Vec<T, N> &&a, const Vec<T, N> &b) noexcept {
	for (auto i = USize{}; i < N; ++i) {
		a[i] -= b[i];
	}
	return a;
}

// Takes a constant reference, and returns a newly created vector.
template <typename T, USize N>
constexpr auto mul(const Vec<T, N> &a, const T &t) noexcept {
	auto b = Vec<T, N>{};
	for (auto i = USize{}; i < N; ++i) {
		b[i] = a[i] * t;
	}
	return b;
}

// Takes a moved vector, mutate it, and returns it.
// This is more memory efficient than taking a copy and returning it,
// and more useful than taking a reference and mutates it.
template <typename T, USize N>
constexpr auto mul(Vec<T, N> &&a, const T &t) noexcept {
	for (auto i = USize{}; i < N; ++i) {
		a[i] *= t;
	}
	return a;
}

template <typename T, USize N>
constexpr auto mul(const Vec<T, N> &a, const Vec<T, N> &b) noexcept {
	auto c = Vec<T, N>{};
	for (auto i = USize{}; i < N; ++i) {
		c[i] = a[i] * b[i];
	}
	return c;
}

template <typename T, USize N>
constexpr auto mul2(Vec<T, N> &a, const T &t) noexcept {
	for (auto i = USize{}; i < N; ++i) {
		a[i] *= t;
	}
}

// TODO(): constexpr sqrt
template <typename T, USize N> auto norm(const Vec<T, N> &a) noexcept {
	auto t = T{};
	for (auto i = USize{}; i < N; ++i) {
		t += a[i] * a[i];
	}
	return fn::sqrt(t);
}

template <typename T, USize N> auto normalize(Vec<T, N> &&a) noexcept {
	auto n = norm(a);
	return mul(std::move(a), 1 / n);
}

template <typename T, USize N> constexpr auto sum(const Vec<T, N> &a) noexcept {
	auto t = T{};
	for (auto i = USize{}; i < N; ++i) {
		t += a[i];
	}
	return t;
}

template <typename T, USize N>
constexpr auto dot(const Vec<T, N> &a, const Vec<T, N> &b) noexcept {
	return sum(mul(a, b));
}

template <typename T>
constexpr auto cross(const Vec<T, 3> &a, const Vec<T, 3> &b) noexcept {
	return Vec<T, 3>{a[1] * b[2] - a[2] * b[1], a[2] * b[0] - a[0] * b[2],
	                 a[0] * b[1] - a[1] * b[0]};
}

} // namespace vec

} // namespace mathpp

#endif /* MATHPP_VEC_HPP */
