#ifndef MATHPP_MAT_HPP
#define MATHPP_MAT_HPP

#include "T.hpp"
#include "Vec.hpp"

#include <utility> /* move() */

namespace mathpp {

// Partial Specialization
// Curiously recurring template pattern
// Static Polymorphism

// template <class T, USize M, USize N = M> using Mat = Vec<Vec<T, N>, M>;

template <typename S, typename S_, typename T, USize M, USize N> class AMat {
private:
	Vec<Vec<T, N>, M> m_{};

protected:
	constexpr AMat() noexcept = default;
	constexpr explicit AMat(Vec<T, N> &&v) noexcept : m_(v) {}
	constexpr explicit AMat(Vec<Vec<T, N>, M> &&m) noexcept
	    : m_{std::move(m)} {}

public:
	// Named constructors
	static constexpr auto empty() noexcept { return S{}; }
	static constexpr auto from_vec1d(Vec<T, N> &&vec1d) noexcept {
		return S{std::move(vec1d)};
	}
	static constexpr auto from_vec2d(Vec<Vec<T, N>, M> &&vec2d) noexcept {
		return S{std::move(vec2d)};
	}

	constexpr auto &operator[](USize i) const noexcept { return m_[i]; };
	constexpr auto &operator[](USize i) noexcept { return m_[i]; };

	constexpr auto transpose() const noexcept {
		auto mm = S_{};
		for (auto i = USize{}; i < M; ++i) {
			for (auto j = USize{}; j < N; ++j) {
				mm[j][i] = (*this)[i][j];
			}
		}
		return mm;
	}

	// template <USize P, USize Q>
	// constexpr void fill(const AMat<T, P, Q> &m) noexcept {
	// 	static_assert(P < M && Q < N, "m2 < m1");
	// 	for (auto i = USize{}; i < P; ++i) {
	// 		for (auto j = USize{}; j < Q; ++j) {
	// 			(*this)[i][j] = m[i][j];
	// 		}
	// 	}
	// }

	// template <USize P, USize Q>
	// constexpr void fit(const AMat<T, P, Q> &m) noexcept {
	// 	static_assert(P > M && Q > N, "m2 > m1");
	// 	for (auto i = USize{}; i < M; ++i) {
	// 		for (auto j = USize{}; j < N; ++j) {
	// 			(*this)[i][j] = m[i][j];
	// 		}
	// 	}
	// }
};

/* Non-square matrix */
template <typename T, USize M, USize N = M>
class Mat : public AMat<Mat<T, M, N>, Mat<T, N, M>, T, M, N> {
	using AMat<Mat<T, M, N>, Mat<T, N, M>, T, M, N>::AMat;

public:
	/* returns a square matrix */
	constexpr auto operator*(const Mat<T, N, M> &m) const noexcept {
		auto mm = Mat<T, M, M>{};
		for (auto i = USize{}; i < M; ++i) {
			for (auto j = USize{}; j < M; ++j) {
				auto t = T{};
				for (auto k = USize{}; k < N; ++k) {
					t += (*this)[i][k] * m[k][j];
				}
				mm[i][j] = t;
			}
		}
		return mm;
	}
};

/* Square matrix */
template <typename T, USize N>
class Mat<T, N, N> : public AMat<Mat<T, N, N>, Mat<T, N, N>, T, N, N> {
	using AMat<Mat<T, N, N>, Mat<T, N, N>, T, N, N>::AMat;

public:
	// static constexpr auto fill(T t) noexcept {
	// 	auto m = Mat{};
	// 	for (auto i = USize{}; i < N; ++i) {
	// 		for (auto j = USize{}; j < N; ++j) {
	// 			m[i][j] = t;
	// 		}
	// 	}
	// 	return m;
	// }

	static constexpr auto identity() noexcept {
		auto m = Mat::empty();
		for (auto i = USize{}; i < N; ++i) {
			m[i][i] = 1;
		}
		return m;
	}

	constexpr void transpose() noexcept {
		for (auto i = USize{}; i < N; ++i) {
			for (auto j = i; j < N; ++j) {
				auto t = (*this)[i][j];
				(*this)[i][j] = (*this)[j][i];
				(*this)[j][i] = t;
			}
		}
	}

	constexpr auto operator*(const Mat &m) const noexcept {
		auto mm = Mat{};
		for (auto i = USize{}; i < N; ++i) {
			for (auto j = USize{}; j < N; ++j) {
				auto t = T{};
				for (auto k = USize{}; k < N; ++k) {
					t += (*this)[i][k] * m[k][j];
				}
				mm[i][j] = t;
			}
		}
		return mm;
	}
};

namespace mat {

template <typename T, USize N> constexpr auto identity() {
	return Mat<T, N>::identity();
}

template <USize P, USize Q = P, typename T, USize M, USize N>
constexpr auto fill(const Mat<T, M, N> &m) noexcept {
	auto mm = Mat<T, P, Q>{};
	static_assert(P < M && Q < N, "m2 < m1");
	for (auto i = USize{}; i < P; ++i) {
		for (auto j = USize{}; j < Q; ++j) {
			mm[i][j] = m[i][j];
		}
	}
	return mm;
}

template <USize P, USize Q = P, typename T, USize M, USize N>
constexpr auto fit(const Mat<T, M, N> &m, T t = {}) noexcept {
	auto mm = Mat<T, P, Q>{};
	static_assert(P > M && Q > N, "m2 > m1");
	for (auto i = USize{}; i < M; ++i) {
		for (auto j = USize{}; j < N; ++j) {
			mm[i][j] = m[i][j];
		}
		for (auto j = N; j < Q; ++j) {
			mm[i][j] = t;
		}
	}
	for (auto i = M; i < P; ++i) {
		for (auto j = USize{}; j < Q; ++j) {
			mm[i][j] = t;
		}
	}
	return mm;
}

} // namespace mat

} // namespace mathpp

#endif /* MATHPP_MAT_HPP */
