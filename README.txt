debugoptimized: -O2 -g
release: -O3 -D NDEBUG
ci: -O3 -g

```sh
mkdir ./build
cd ./build
meson setup -D b_lto=true -D b_pie=true -D buildtype=release . ..
samu
samu test
samu install
```
