[binaries]
c = 'clang'
cpp = 'clang++'
c_ld = 'lld'
cpp_ld = 'lld'
ar = 'llvm-ar'
objcopy = 'llvm-objcopy'
strip = 'llvm-strip'
# clang uses integrated assembler (llvm-as) by default

[built-in options]
c_args = []
c_link_args = ['--rtlib=compiler-rt', '--unwindlib=libunwind']
cpp_args = ['--stdlib=libc++']
# --unwindlib=libgcc: https://bugs.llvm.org/show_bug.cgi?id=16404#c25
cpp_link_args = ['--rtlib=compiler-rt', '--unwindlib=libunwind', '--stdlib=libc++']
